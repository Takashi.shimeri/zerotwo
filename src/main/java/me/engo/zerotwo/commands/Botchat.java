package me.engo.zerotwo.commands;

import com.google.gson.JsonParser;
import me.engo.zerotwo.Config;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

public class Botchat extends ListenerAdapter {

    public static String alias = "chat";

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        Config c = new Config();
        String[] args = event.getMessage().getContentRaw().split(" ");
        if (event.getAuthor().isBot()) return;

        if (args[0].equalsIgnoreCase(c.prefix + "botchat") || args[0].equalsIgnoreCase(c.prefix + alias)) {

            try {
                String language;
                File languages = new File("Database/Language/" + event.getAuthor().getId());
                if (languages.exists()) {
                    File[] languages_ = languages.listFiles();
                    assert languages_ != null;
                    language = languages_[0].getName();
                } else {
                    language = "english_en";
                }

                if (args.length < 2) {
                    String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("low_parameters").getAsString();
                    event.getChannel().sendMessage(text).queue();
                } else {
                    if (!Objects.requireNonNull(event.getMember()).hasPermission(Permission.MESSAGE_MANAGE)){
                        String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("permissions_false").getAsString();
                        event.getChannel().sendMessage(text).queue();
                        return;
                    }
                    File f = new File("Database/Botchat/" + event.getGuild().getId());

                    if (args[1].equalsIgnoreCase("on")){
                        //ZAPNUTÍ
                            if (args.length == 3){
                                //POKUD JE VŠE SPRÁVNĚ
                                if (f.mkdir()) {
                                    //POKUD JEŠTĚ NEEXISTUJE

                                    TextChannel channel = event.getMessage().getMentionedChannels().get(0);

                                    Files.createFile(Paths.get(f.getPath() + "/" + channel.getId()));

                                    String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("botchat_enabled").getAsString();
                                    event.getChannel().sendMessage(text).queue();
                                } else {
                                    //POKUD JIŽ EXISTUJE
                                    String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("botchat_err_1").getAsString();
                                    event.getChannel().sendMessage(text).queue();
                                }
                            } else {
                                if (args.length < 4){
                                    String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("low_parameters").getAsString();
                                    event.getChannel().sendMessage(text).queue();
                                } else {
                                    String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("high_parameters").getAsString();
                                    event.getChannel().sendMessage(text).queue();
                                }
                            }
                    } else if (args[1].equalsIgnoreCase("off")){
                        //VYPNUTÍ
                        if (args.length == 2){
                            if (f.exists()){
                                //POKUD EXISTUJE
                                for (File file : Objects.requireNonNull(f.listFiles())){
                                    Files.delete(file.toPath());
                                }
                                Files.delete(f.toPath());

                                String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("botchat_disabled").getAsString();
                                event.getChannel().sendMessage(text).queue();
                            } else {
                                //POKUD NEEXISTUJE
                                String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("botchat_err_2").getAsString();
                                event.getChannel().sendMessage(text).queue();
                            }
                        } else {
                            String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("high_parameters").getAsString();
                            event.getChannel().sendMessage(text).queue();
                        }
                    } else {
                        String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("advanced_warnings").getAsJsonObject().get("invalid_parameter").getAsString();
                        event.getChannel().sendMessage("'" + args[1] + text).queue();
                    }
                }

            } catch (IOException e){
                e.printStackTrace();
            }
        }
    }

}
