package me.engo.zerotwo.commands;

import com.google.gson.JsonParser;
import me.engo.zerotwo.Config;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Objects;

public class Kick extends ListenerAdapter {

    public static String alias = "out";

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        Config c = new Config();
        String[] args = event.getMessage().getContentRaw().split(" ");
        if (event.getAuthor().isBot()) return;

        if (args[0].equalsIgnoreCase(c.prefix + "kick") || args[0].equalsIgnoreCase(c.prefix + alias)) {

            try {
                String language;
                File languages = new File("Database/Language/" + event.getAuthor().getId());
                if (languages.exists()) {
                    File[] languages_ = languages.listFiles();
                    assert languages_ != null;
                    language = languages_[0].getName();
                } else {
                    language = "english_en";
                }

                if (args.length < 2) {
                    String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("low_parameters").getAsString();
                    event.getChannel().sendMessage(text).queue();
                } else if (args.length == 2) {
                    if (c.moderation) {
                        if (Objects.requireNonNull(event.getMember()).getPermissions(event.getChannel()).contains(Permission.KICK_MEMBERS)) {
                            Member u = event.getMessage().getMentionedMembers().get(0);

                            if (u == null){
                                try {
                                    String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("low_parameters").getAsString();
                                    event.getChannel().sendMessage(text).queue();
                                    return;
                                } catch (FileNotFoundException ex) {
                                    ex.printStackTrace();
                                }
                            }

                            assert u != null;
                            u.kick().queue();
                            String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("was_kicked_template").getAsString();
                            Objects.requireNonNull(event.getGuild().getDefaultChannel()).sendMessage(u.getUser().getName() + text + event.getAuthor().getName() + ".").queue();
                        } else {
                            String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("permissions_false").getAsString();
                            event.getChannel().sendMessage(text).queue();
                        }
                    } else {
                        String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("moderationmodule_false").getAsString();
                        event.getChannel().sendMessage(text).queue();
                    }
                } else {
                    if (c.moderation) {
                        if (Objects.requireNonNull(event.getMember()).getPermissions(event.getChannel()).contains(Permission.KICK_MEMBERS)) {
                            Member m = event.getMessage().getMentionedMembers().get(0);

                            if (m == null){
                                try {
                                    String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("low_parameters").getAsString();
                                    event.getChannel().sendMessage(text).queue();
                                    return;
                                } catch (FileNotFoundException ex) {
                                    ex.printStackTrace();
                                }
                            }

                            assert m != null;

                            int a = args.length;

                            String[] slova = new String[a];

                            System.arraycopy(args, 1, slova, 0, a - 1);

                            StringBuilder done = new StringBuilder();

                            for (int i = 1; i < slova.length - 1; i++) {
                                done.append(" ").append(slova[i]);
                            }

                            done = new StringBuilder(done.substring(1));


                            m.kick(done.toString()).queue();
                            String text1 = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("was_kicked_template").getAsString();
                            String text2 = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("was_kicked_for").getAsString();
                            Objects.requireNonNull(event.getGuild().getDefaultChannel()).sendMessage(m.getUser().getName() + text1 + event.getAuthor().getName() + text2 + done + ".").queue();
                        } else {
                            String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("permissions_false").getAsString();
                            event.getChannel().sendMessage(text).queue();
                        }
                    } else {
                        String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("permissions_false").getAsString();
                        event.getChannel().sendMessage(text).queue();
                    }
                }
            } catch (FileNotFoundException e){
                e.printStackTrace();
            }
        }
    }
}
