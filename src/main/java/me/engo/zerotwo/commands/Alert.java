package me.engo.zerotwo.commands;

import com.google.gson.JsonParser;
import me.engo.zerotwo.Config;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class Alert extends ListenerAdapter {

    public static String alias = "There's no aliases";

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        Config c = new Config();
        String[] args = event.getMessage().getContentRaw().split(" ");
        if (event.getAuthor().isBot()) return;

        if (args[0].equalsIgnoreCase(c.prefix + "alert")) {

            try {
                String language;
                File languages = new File("Database/Language/" + event.getAuthor().getId());
                if (languages.exists()) {
                    File[] languages_ = languages.listFiles();
                    assert languages_ != null;
                    language = languages_[0].getName();
                } else {
                    language = "english_en";
                }

                if (event.getAuthor().getId().equalsIgnoreCase("574992310048260097")){
                    if (args.length < 2) {
                        String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("low").getAsString();
                        event.getChannel().sendMessage(text).queue();
                    } else {
                        int a = args.length;

                        String[] slova = new String[a];

                        System.arraycopy(args, 1, slova, 0, a - 1);

                        StringBuilder done = new StringBuilder();

                        for (int i = 0; i < slova.length - 1; i++) {
                            done.append(" ").append(slova[i]);
                        }

                        done = new StringBuilder(done.substring(1));

                        StringBuilder finalDone = done;
                        event.getChannel().sendMessage("***Processing...***").queue(message -> {
                            for (Guild g : event.getJDA().getGuilds()){
                                File f = new File("Database/Updates/" + g.getId());
                                TextChannel tc;
                                if (f.exists()){
                                    File f1 = new File("Database/Updates/" + g.getId() + "/Channel");
                                    File[] files = f1.listFiles();
                                    assert files != null;
                                    tc = g.getTextChannelById(files[0].getName());
                                } else {
                                    tc = g.getDefaultChannel();
                                }

                                if (tc != null && g.getSelfMember().getPermissions(tc).contains(Permission.MESSAGE_WRITE)){
                                    tc.sendMessage(finalDone.toString()).queue();
                                }
                            }
                            message.editMessage("**Done.**").queue();
                        });
                    }
                } else {
                    String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("developer_false").getAsString();
                    event.getChannel().sendMessage(text).queue();
                }
            } catch (FileNotFoundException e){
                e.printStackTrace();
            }
        }
    }

}
