package me.engo.zerotwo.commands;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Random;

import com.google.gson.JsonParser;
import me.engo.zerotwo.Config;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class Flip extends ListenerAdapter {
	
	public static String alias = "coin";
	public String side;
	
	@Override
	public void onGuildMessageReceived(GuildMessageReceivedEvent Context) {
		Config c = new Config();
		String[] messageSent = Context.getMessage().getContentRaw().split(" ");
		if (Context.getAuthor().isBot()) return;
		
		if (messageSent[0].equalsIgnoreCase(c.prefix + "flip") || messageSent[0].equalsIgnoreCase(c.prefix + alias)) {

			try {
				String language;
				File languages = new File("Database/Language/" + Context.getAuthor().getId());
				if (languages.exists()) {
					File[] languages_ = languages.listFiles();
					assert languages_ != null;
					language = languages_[0].getName();
				} else {
					language = "english_en";
				}

				if (messageSent.length < 2) {
					String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("low_parameters").getAsString();
					Context.getChannel().sendMessage(text).queue();
				} else if (messageSent.length == 2) {
					if (!(messageSent[1].toLowerCase().equals("tail")) && !(messageSent[1].toLowerCase().equals("head"))) {
						System.out.println(messageSent[1]);
						String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("advanced_warnings").getAsJsonObject().get("invalid_coin_side").getAsString();
						Context.getChannel().sendMessage(text + c.prefix + "flip [head/tail])").queue();
					} else {
						Random rnd = new Random();
						int rndm = rnd.nextInt(10);

						if (rndm <= 5) {
							side = messageSent[1].toLowerCase();
							String text1 = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("flip_template").getAsString();
							String text2 = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("flip_won").getAsString();
							Context.getChannel().sendMessage(text1 + side + text2).queue();
						} else {
							if (messageSent[1].toLowerCase().equals("head")) {
								side = "tail";
							}
							if (messageSent[1].toLowerCase().equals("tail")) {
								side = "head";
							}
							String text1 = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("flip_template").getAsString();
							String text2 = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("flip_lost").getAsString();
							Context.getChannel().sendMessage(text1 + side + text2).queue();
						}
					}
				} else {
					String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("high_parameters").getAsString();
					Context.getChannel().sendMessage(text).queue();
				}
			} catch (FileNotFoundException e){
				e.printStackTrace();
			}
		}
	}

}
