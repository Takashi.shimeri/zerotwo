package me.engo.zerotwo.commands;

import com.google.gson.JsonParser;
import me.engo.zerotwo.Config;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Objects;

public class Channelcreate extends ListenerAdapter {
	
public static String alias = "cc";
	
	@Override
	public void onGuildMessageReceived(GuildMessageReceivedEvent Context) {
		Config c = new Config();
		String[] messageSent = Context.getMessage().getContentRaw().split(" ");
		if (Context.getAuthor().isBot()) return;
		
		if (messageSent[0].equalsIgnoreCase(c.prefix + "channelcreate") || messageSent[0].equalsIgnoreCase(c.prefix + alias)) {

			try {
				String language;
				File languages = new File("Database/Language/" + Context.getAuthor().getId());
				if (languages.exists()) {
					File[] languages_ = languages.listFiles();
					assert languages_ != null;
					language = languages_[0].getName();
				} else {
					language = "english_en";
				}

				if (messageSent.length < 3) {
					String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("advanced_warnings").getAsJsonObject().get("template_channel").getAsString();
					Context.getChannel().sendMessage(text + c.prefix + "channelcreate general text`").queue();
				} else if (messageSent.length > 3) {
					String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("high_parameters").getAsString();
					Context.getChannel().sendMessage(text).queue();
				} else {
					if (c.moderation) {
						if (Objects.requireNonNull(Context.getMember()).getPermissions(Context.getChannel()).contains(Permission.MANAGE_CHANNEL)) {
							if (messageSent[2].toLowerCase().equals("text") || messageSent[2].toLowerCase().equals("voice")) {
								if (messageSent[2].equals("text")) {
									Context.getGuild().createTextChannel(messageSent[1]).queue();
								} else {
									Context.getGuild().createVoiceChannel(messageSent[1]).queue();
								}
								String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("channel_created").getAsString();
								Context.getChannel().sendMessage(text + messageSent[1].toLowerCase() + "'.").queue();
							} else {
								String text1 = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("advanced_warnings").getAsJsonObject().get("invalid_channel_type_1").getAsString();
								String text2 = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("advanced_warnings").getAsJsonObject().get("invalid_channel_type_2").getAsString();
								Context.getChannel().sendMessage(text1 + messageSent[2] + text2).queue();
							}
						} else {
							String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("permissions_false").getAsString();
							Context.getChannel().sendMessage(text).queue();
						}
					} else {
						String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("moderationmodule_false").getAsString();
						Context.getChannel().sendMessage(text).queue();
					}
				}
			} catch (FileNotFoundException e){
				e.printStackTrace();
			}
		}
	}
}
