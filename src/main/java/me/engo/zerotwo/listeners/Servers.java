package me.engo.zerotwo.listeners;

import me.engo.zerotwo.Bot;
import net.dv8tion.jda.api.events.guild.GuildJoinEvent;
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class Servers extends ListenerAdapter {

    @Override
    public void onGuildJoin(GuildJoinEvent event) {
        Bot.api.setStats(event.getJDA().getGuilds().size());
        //setCount(Bot.token_3, event.getJDA().getGuilds().size());
    }

    @Override
    public void onGuildLeave(GuildLeaveEvent event) {
        Bot.api.setStats(event.getJDA().getGuilds().size());
        //setCount(Bot.token_3, event.getJDA().getGuilds().size());
    }

    /*public static void setCount(String token, int size){
        try {
            ScriptEngineManager manager = new ScriptEngineManager();
            ScriptEngine engine = manager.getEngineByName("JavaScript");

            engine.eval(Files.newBufferedReader(Paths.get("index.js"), StandardCharsets.UTF_8));
            Invocable inv = (Invocable) engine;
            inv.invokeFunction("start", token, size);
        } catch (ScriptException | NoSuchMethodException | IOException e) {
            e.printStackTrace();
        }
    }*/
}
